//
//  FacebookAPI.swift
//  Enerj
//
//  Created by Manuel Loigeret on 2016-08-04.
//  Copyright © 2016 SPARQ Studio. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit

class FacebookAPI: NSObject {
    /*
     * Checks if the user is logged in
     */
    class func isLoggedIn() -> Bool {
        
        if let token = FBSDKAccessToken.currentAccessToken(){
            print("Logged in... \(token.tokenString)")
            return true
        }
        
        print("Not logged in")
        return false
        
    }
    
    
    /*
     * Login process
     */
    
    class func loginToFacebookFromViewController( viewController:UIViewController, successBlock: () -> (), andFailure failureBlock: (NSError?) -> ()) {
        
        let readPermissions = ["public_profile", "email", "user_friends"]
        
        FBSDKLoginManager().logInWithReadPermissions(readPermissions, fromViewController: viewController) { (result:FBSDKLoginManagerLoginResult!, error:NSError!) in
            
            if error != nil {
                FBSDKLoginManager().logOut()
                failureBlock(error)
            } else if result.isCancelled {
                FBSDKLoginManager().logOut()
                failureBlock(nil)
            } else {
                successBlock()
                
            }
        }
    }
    
    
    /*
     * Logout process
     */
    
    class func logOut(){
        FBSDKLoginManager().logOut() 
    }

}
