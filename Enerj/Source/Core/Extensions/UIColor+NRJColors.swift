//
//  UIColor+NRJColors.swift
//  Enerj
//
//  Created by Manuel Loigeret on 2016-08-09.
//  Copyright © 2016 SPARQ Studio. All rights reserved.
//

import UIKit
import UIColor_Hex_Swift

extension UIColor{
    

    class func nrjPurple() -> UIColor{
        return UIColor(rgba: "#74007f")
    }
    

    
}

