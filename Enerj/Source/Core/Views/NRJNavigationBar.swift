//
//  NRJNavigationBar.swift
//  Enerj
//
//  Created by Manuel Loigeret on 2016-08-09.
//  Copyright © 2016 SPARQ Studio. All rights reserved.
//

import UIKit

class NRJNavigationBar: UINavigationBar {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initialize()
        
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialize()
    }
    
    
    
    func initialize(){
        self.barTintColor = UIColor.nrjPurple()
        self.tintColor = UIColor.whiteColor()
        self.translucent = false

        var attributes = [String:AnyObject]()
        attributes[NSForegroundColorAttributeName]=UIColor.whiteColor()
        attributes[NSBackgroundColorAttributeName]=UIColor.whiteColor()
        self.titleTextAttributes=attributes
        
        
       /* [self setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:kDefaultBoldFontName size:17.0f],NSFontAttributeName,[UIColor whiteColor],NSForegroundColorAttributeName,
            [UIColor whiteColor],NSBackgroundColorAttributeName, nil]];*/
    
    }
    

}
