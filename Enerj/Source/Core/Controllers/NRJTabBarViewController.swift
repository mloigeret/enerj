//
//  NRJTabBarViewController.swift
//  Enerj
//
//  Created by Manuel Loigeret on 2016-08-04.
//  Copyright © 2016 SPARQ Studio. All rights reserved.
//

import UIKit

class NRJTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        UITabBar.appearance().tintColor=UIColor.nrjPurple()
        
        guard let items = self.tabBar.items
            else {return}
        
        for item in items {
            item.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0)
        }
        
        //items[0].image = UIImage(named: "feedTab");
        
        
        
        /*
 
         //tint
         [[UITabBar appearance] setTintColor:UIColorFromRGB(0x00B3C9)];
         
         //center image
         for(UITabBarItem * item in self.tabBar.items)
         item.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
         
         //custom unselected / selected images
         self.tabBar.items[0].image = [UIImage imageNamed:@"matchesTab"];
         self.tabBar.items[0].selectedImage = [UIImage imageNamed:@"matchesTabSelected"];
         self.tabBar.items[1].image = [UIImage imageNamed:@"activityTab"];
         self.tabBar.items[1].selectedImage = [UIImage imageNamed:@"activityTabSelected"];
         self.tabBar.items[2].image = [UIImage imageNamed:@"matchmakerTab"];
         self.tabBar.items[2].selectedImage = [UIImage imageNamed:@"matchmakerTabSelected"];
         self.tabBar.items[3].image = [UIImage imageNamed:@"dashboardTab"];
         self.tabBar.items[3].selectedImage = [UIImage imageNamed:@"dashboardTabSelected"];
         self.tabBar.items[4].image = [UIImage imageNamed:@"profileTab"];
         self.tabBar.items[4].selectedImage = [UIImage imageNamed:@"profileTabSelected"];
 
 
 
 */
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
