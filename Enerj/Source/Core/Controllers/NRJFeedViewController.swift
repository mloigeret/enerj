//
//  NRJFeedViewController.swift
//  Enerj
//
//  Created by Manuel Loigeret on 2016-08-04.
//  Copyright © 2016 SPARQ Studio. All rights reserved.
//

import UIKit

class NRJFeedViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func configureTableView(){
        
        let nib = UINib(nibName: "NRJFeedTableViewCell", bundle: nil)
        tableView .registerNib(nib, forCellReuseIdentifier: "NRJFeedTableViewCell")

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
